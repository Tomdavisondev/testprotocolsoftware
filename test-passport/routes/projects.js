const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');

// Project model
const Project = require('../models/Project')

//New Test Case Handle
router.post('/:projectName/addtestcase', (req, res) => {
    const {testcasename} = req.body;
    let errors = [];
    //console.log(req.params.projectName);
    //console.log(req.body);
	let projectname = req.params.projectName;

    if (!testcasename) {
        errors.push({msg: 'Something went wrong, could not find project'});
    }

    if (errors.length > 0) {
        res.render('dashboard', {
            errors,
            projectname
        });
    } else {
        Project.findOne({projectname: projectname})
            .then(project => {
                if (project) {
                	Project.update(
                		{ _id: project._id },
						{ $push: { name: testcasename } })
						.then(project => {
							//Project exists
							req.flash('error_msg', "Project already exists under that name")
							res.render('dashboard', {
								errors,
								name,
								projectname
							})
						.catch(err => console.log(err));
						})
                } else {
					req.flash('error_msg', "Project does not exist");
					res.render('dashboard', {
						errors,
						projectname
					});
                }
            })
    }
});

//New Project Handle
router.post('/register', (req, res) => {
    const {projectname} = req.body;
    let errors = [];

    if (!projectname) {
        errors.push({msg: 'Please fill in all fields'});
    }

    if (errors.length > 0) {
        res.render('dashboard', {
            errors,
            projectname
        });
    } else {
        Project.findOne({projectname: projectname})
            .then(project => {
                if (project) {
                    //Project exists
                    req.flash('error_msg', "Project already exists under that name")
                    res.render('dashboard', {
                        errors,
                        name,
                        projectname
                    });
                } else {
                    const newProject = new Project({
                        projectname,
                    });
                    newProject.save()
                        .then(project => {
                            req.flash('success_msg', "New project created");
                            res.redirect('../dashboard');
                        })
                        .catch(err => console.log(err));
                }
            })
    }
});

module.exports = router;