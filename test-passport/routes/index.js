const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');

const Project = require('../models/Project');

router.get('/project/:projectName', ensureAuthenticated, (req, res) => {
	Project.find({}).exec(function(err, projects) {
		Project.findOne({projectname: req.params.projectName})
			.then(project => {
				if (project) {
					res.render('project', {
						name: req.user.name,
						project: project,
						projects: projects
					})
				} else {
					//TODO: Proper validation when a project is not found
					// this shouldn't ever happen but eh, worth doing
					console.log("Error: project not found error");
					console.log(req.projectName);
					console.log(req.params);
				}
			});
	});
});

// Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) =>
	Project.find({}).exec(function(err, projects) {
		if (err) throw err;
		res.render('dashboard', {
			name: req.user.name,
			projects: projects
		})
	}));

module.exports = router;