const mongoose = require('mongoose');

const ProjectSchema = new mongoose.Schema({
	projectname: {
		type: String,
		required: true
	},
	testcases: [
		{
			name: String
		}
	],
});

const Project = mongoose.model('Project', ProjectSchema);

module.exports = Project;